#!/usr/bin/python

import inkscapeMadeEasy.inkscapeMadeEasy_Base as inkBase
import inkscapeMadeEasy.inkscapeMadeEasy_Draw as inkDraw


class blackbox(inkBase.inkscapeMadeEasy):

    # ---------------------------------------------
    def drawBlackBoxLblText(self,e,position,side,rotation,label,uselatex):
        """ Draws the text for a label for a black box.

        e: element of upper group object so each line and label gets its own
        position: position [x,y]
        side: side of black box
        rotation: label rotation
        label: text for the label
        """

        text_adjust = 2

        if side=='input':
            refPoint='cr'
            name_just = 'right'
            pos_text = position[0]-self.const.line_width/2.0, position[1] - self.const.lbl_height/2.0
            if not uselatex:
                pos_text = pos_text[0], pos_text[1] + text_adjust


        elif side=='output':
            refPoint='cl'
            name_just = 'left'
            pos_text = position[0]+self.const.line_width/2.0, position[1] - self.const.lbl_height/2.0
            if not uselatex:
                pos_text = pos_text[0], pos_text[1] + text_adjust

        elif side=='top':
            refPoint='cl'
            name_just = 'left'
            pos_text = position[0] - self.const.lbl_height, position[1]-self.const.line_width/2.0
            if not uselatex:
                pos_text = pos_text[0] + text_adjust, pos_text[1]

        elif side=='bottom':
            refPoint='cr'
            name_just = 'right'
            pos_text = position[0] - self.const.lbl_height, position[1]+self.const.line_width/2.0
            if not uselatex:
                pos_text = pos_text[0] + text_adjust, pos_text[1]

        # If Bus
        if label and label[0] == '#':
            label = label[1:]

        # Text
        if label:
            if uselatex:
                g = inkDraw.text.latex(self,e,label,pos_text,fontSize=self.fontSize,refPoint=refPoint,preambleFile=self.preambleFile,angleDeg=rotation)
            else:
                g = inkDraw.text.write(self, label, pos_text, e, justification=name_just, fontSize=self.fontSize*1.5, angleDeg=rotation, textStyle=self.const.textStyle)

            bmin, bmax = self.getBoundingBox(g)
        else:
            bmin = 0,0
            bmax = 0,0

        return bmin, bmax

    #---------------------------------------------
    def drawBlackBoxLbl(self,e,position,side,rotation,label,long_length):
        """ Draws a label for a black box.

        e: element group to store this line and label text in
        position: position [x,y]
        side: side of black box
        rotation: label rotation
        label: text for the label
        long_length: length of the long lines
        """

        if side=='input':
            from_coord = position[0]+self.const.line_width/2.0, position[1] + self.const.lbl_height/2.0
            from_long_coord = position[0], position[1] + self.const.lbl_height/2.0
            to_coord = position[0]-self.const.line_width/2.0, position[1] + self.const.lbl_height/2.0
            long_coord = position[0]-long_length-self.const.line_width, to_coord[1]

        elif side=='output':
            from_coord = position[0]-self.const.line_width/2.0, position[1] + self.const.lbl_height/2.0
            from_long_coord = position[0], position[1] + self.const.lbl_height/2.0
            to_coord = position[0]+self.const.line_width/2.0, position[1] + self.const.lbl_height/2.0
            long_coord = position[0]+long_length+self.const.line_width, to_coord[1]

        elif side=='top':
            from_coord = position[0], position[1]+self.const.line_width/2.0
            from_long_coord = position[0], position[1]
            to_coord = position[0], position[1]-self.const.line_width/2.0
            long_coord = to_coord[0], position[1]-long_length - self.const.line_width

        elif side=='bottom':
            from_coord = position[0], position[1]-self.const.line_width/2.0
            from_long_coord = position[0], position[1]
            to_coord = position[0], position[1]+self.const.line_width/2.0
            long_coord = to_coord[0], position[1]+long_length+self.const.line_width

        # Line Styles
        long_style = {
                                    'fill': 'none',
                                    'stroke': '#2CBA15',
                                    'stroke-dasharray': 'none',
                                    'stroke-linecap': 'square',
                                    'stroke-linejoin': 'miter',
                                    'stroke-width': 1.0
                                 }
        short_style = {
                                    'fill': 'none',
                                    'stroke': '#000000',
                                    'stroke-dasharray': 'none',
                                    'stroke-linecap': 'square',
                                    'stroke-linejoin': 'miter',
                                    'stroke-width': 1.0
                                 }

        # If Bus
        if label and label[0] == '#':
            label = label[1:]
            long_style['stroke-width'] = 2.0
            long_style['stroke'] = '#008000'
            short_style['stroke-width'] = 2.0

        # Long Line
        inkDraw.line.absCoords(e,[from_long_coord, long_coord], lineStyle=long_style)

        # Short Line
        inkDraw.line.absCoords(e,[from_coord, to_coord], lineStyle=short_style)

    #---------------------------------------------
    def drawBlackBox(self,parent,position,name,inputString,outputString,topString,bottomString,io_symm,tb_symm,uselatex,label="Blackbox"):
        """ Draws a black box.

        parent: parent object
        position: position [x,y]
        label: label of the object (it can be repeated)
        name: string with the name of the black box.
        inputString: comma separated list for the left side inputs
        outputString: comma separated list for the right side outputs
        topString: comma separated list for the top signals
        bottomString: comma separated list for the bottom signals
        io_symm: make input and output long lines the same length
        tb_symm: make top and bottom long lines the same length
        uselatex: use latex to make the labels
        """

        # Constants
        self.const.size_mult=20                    # Size multiple for number of signals
        self.const.name_sep=6                        # Separate the name from the box
        self.const.min_size=2                        # Minimum size of box side
        self.const.long_length_bound=10    # Minimum long line length
        self.const.line_width=10                 # Width of short line
        self.const.lbl_height=5                    # Signal group height
        self.const.textStyle = {                 # Text style
                                                            'fill': '#000000',
                                                            'fill-opacity': '1',
                                                            'font-family': 'Times New Roman',
                                                            'font-size': '12px',
                                                            'font-style': 'normal',
                                                            'font-weight': 'normal',
                                                            'letter-spacing': '0px',
                                                            'line-height': '100%',
                                                            'stroke': 'none',
                                                            'text-align': 'start',
                                                            'text-anchor': 'start',
                                                            'word-spacing': '0px'
                                                     }

        # Group
        group = self.createGroup(parent,label)

        # Turn label strings into lists
        if inputString:
            inputs = inputString.split(',')
            inputs = [i.strip() for i in inputs]
        else:
            inputs = []

        if outputString:
            outputs = outputString.split(',')
            outputs = [i.strip() for i in outputs]
        else:
            outputs = []

        if topString:
            tops = topString.split(',')
            tops = [i.strip() for i in tops]
        else:
            tops = []

        if bottomString:
            bottoms = bottomString.split(',')
            bottoms = [i.strip() for i in bottoms]
        else:
            bottoms = []

        # Calculate width and height
        width = max(len(tops),len(bottoms),self.const.min_size)*self.const.size_mult
        height = max(len(inputs),len(outputs),self.const.min_size)*self.const.size_mult

        # Draw Name
        if bottoms:
            pos_text = position[0],position[1]+self.const.lbl_height/2
            name_ref='cc'
            name_just='center'
        else:
            pos_text = position[0],position[1]+height/2.0+self.const.name_sep
            name_ref='tc'
            name_just='center'

        if uselatex:
            g = inkDraw.text.latex(self, group, '\\large\\textbf{'+name+'}', pos_text, refPoint=name_ref, preambleFile=self.preambleFile, fontSize=self.fontSize,)
        else:
            tStyle = self.const.textStyle.copy()
            tStyle['font-weight'] = 'bold'
            pos_text =    pos_text[0], pos_text[1] + 6
            g = inkDraw.text.write(self, name, pos_text, group, justification=name_just, fontSize=self.fontSize*1.5*1.3, textStyle=tStyle)

        # Calculate how wide the box is going to be if we're moving it to the middle
        if bottoms:
            bmin, bmax = self.getBoundingBox(g)
            width = max(width, bmax[0]-bmin[0]+2*self.const.lbl_height+2*self.const.name_sep)

        # Draw Box
        box_style = {
                                    'fill': '#DFEBF8',
                                    'stroke': '#000000',
                                    'stroke-dasharray': 'none',
                                    'stroke-linecap': 'butt',
                                    'stroke-linejoin': 'miter',
                                    'stroke-width': '1.0'
                                }
        inkDraw.rectangle.widthHeightCenter(group, position, width, height,lineStyle=box_style)

        # Remove text and redraw if it is going in the center
        if bottoms:
            self.removeElement(g)

            if uselatex:
                inkDraw.text.latex(self,group,'\\large\\textbf{'+name+'}',pos_text,refPoint=name_ref,preambleFile=self.preambleFile,fontSize=self.fontSize,)
            else:
                inkDraw.text.write(self, name, pos_text, group, justification=name_just, fontSize=self.fontSize*1.5*1.3, textStyle=tStyle)

        # Draw input labels
        elements1 = []
        lengths1 = []
        for x,i in enumerate(inputs):
            e = self.createGroup(group)
            lheight = len(inputs)*self.const.size_mult
            pos_line = position[0]-width/2.0, position[1]-lheight/2.0+(x+0.5)*self.const.size_mult
            bmin, bmax = self.drawBlackBoxLblText(e,pos_line,'input',0,i,uselatex)
            lengths1.append(bmax[0]-bmin[0])
            elements1.append(e)

        # Draw output labels
        elements2 = []
        lengths2 = []
        for x,i in enumerate(outputs):
            e = self.createGroup(group)
            lheight = len(outputs)*self.const.size_mult
            pos_line = position[0]+width/2.0, position[1]-lheight/2.0+(x+0.5)*self.const.size_mult
            bmin, bmax = self.drawBlackBoxLblText(e,pos_line,'output',0,i,uselatex)
            lengths2.append(bmax[0]-bmin[0])
            elements2.append(e)

        if lengths1:
            long_length1 = max(lengths1)
        else:
            long_length1 = 0
        if lengths2:
            long_length2 = max(lengths2)
        else:
            long_length2 = 0

        # Draw input label lines
        if io_symm:
            long_length = max(long_length1, long_length2)
        else:
            long_length = long_length1
        long_length=max(long_length, self.const.long_length_bound)
        for x,i in enumerate(inputs):
            lheight = len(inputs)*self.const.size_mult
            pos_line = position[0]-width/2.0, position[1]-lheight/2.0+(x+0.5)*self.const.size_mult
            self.drawBlackBoxLbl(elements1[x],pos_line,'input',0,i,long_length)

        # Draw output label lines
        if io_symm:
            long_length = max(long_length1, long_length2)
        else:
            long_length = long_length2
        long_length=max(long_length, self.const.long_length_bound)
        for x,i in enumerate(outputs):
            lheight = len(outputs)*self.const.size_mult
            pos_line = position[0]+width/2.0, position[1]-lheight/2.0+(x+0.5)*self.const.size_mult
            self.drawBlackBoxLbl(elements2[x],pos_line,'output',0,i,long_length)

        # Draw top labels
        elements1 = []
        lengths1 = []
        for x,i in enumerate(tops):
            e = self.createGroup(group)
            lwidth = len(tops)*self.const.size_mult
            pos_line = position[0]-lwidth/2.0+(x+0.5)*self.const.size_mult, position[1]-height/2.0
            bmin, bmax = self.drawBlackBoxLblText(e,pos_line,'top',90,i,uselatex)
            lengths1.append(bmax[1]-bmin[1])
            elements1.append(e)

        # Draw bottom labels
        elements2 = []
        lengths2 = []
        for x,i in enumerate(bottoms):
            e = self.createGroup(group)
            lwidth = len(bottoms)*self.const.size_mult
            pos_line = position[0]-lwidth/2.0+(x+0.5)*self.const.size_mult, position[1]+height/2.0
            bmin, bmax = self.drawBlackBoxLblText(e,pos_line,'bottom',90,i,uselatex)
            lengths2.append(bmax[1]-bmin[1])
            elements2.append(e)

        if lengths1:
            long_length1 = max(lengths1)
        else:
            long_length1 = 0
        if lengths2:
            long_length2 = max(lengths2)
        else:
            long_length2 = 0

        # Draw top label lines
        if tb_symm:
            long_length = max(long_length1, long_length2)
        else:
            long_length = long_length1
        long_length=max(long_length, self.const.long_length_bound)
        for x,i in enumerate(tops):
            lwidth = len(tops)*self.const.size_mult
            pos_line = position[0]-lwidth/2.0+(x+0.5)*self.const.size_mult, position[1]-height/2.0
            self.drawBlackBoxLbl(elements1[x],pos_line,'top',90,i,long_length)

        # Draw bottom label lines
        if tb_symm:
            long_length = max(long_length1, long_length2)
        else:
            long_length = long_length2
        long_length=max(long_length, self.const.long_length_bound)
        for x,i in enumerate(bottoms):
            lwidth = len(bottoms)*self.const.size_mult
            pos_line = position[0]-lwidth/2.0+(x+0.5)*self.const.size_mult, position[1]+height/2.0
            self.drawBlackBoxLbl(elements2[x],pos_line,'bottom',90,i,long_length)

        return group




